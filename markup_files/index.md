![Bild 1](media/1.jpeg)

**3D-modellering av grund och olika slags ledningar ger ett säkrare beslutsunderlag och bättre tredimensionell samgranskning av ledningar. Eventuella problem löses gemensamt.**

# Ökad säkerhet genom ledningssamordning i 3D

> ##### I samband med bostadsprojektet Örtedalen i Uppsala används BIM i husprojekteringen. 3D-projektering har även fått stor betydelse när det gäller markfrågorna. För att minimera riskerna med grundläggningen i projektet har hela grunden med olika slags ledningar och grundläggningar modellerats i 3D. Erfarenheterna är mycket positiva.

– DET ÄR FÖRSTA GÅNGEN VI ANVÄNDER BIM FÖR markfrågor och jag ser bara fördelar, säger Klas Eckerberg, landskapsarkitekt på Bjerking AB. Vårt arbete med Örtedalen är en påminnelse om att BIM inte bara är användbart för byggnader utan i högsta grad motiverat för hantering av markfrågor.
​	Örtedalen är ett partneringprojekt mellan Bjerking, NCC Boende och NCC Construction, som ska leda fram till ett exklusivt bostadshus med 36 bostadsrättslägenheter och innergård beläget i centrala Uppsala. Kvalitetsambitionerna är höga. Partneringprojektet innebär ett tätt samarbete mellan
projektörer, entreprenörer och säljare.
​	Grundläggningsförhållandena är svåra med uppfylld mark och stort pålningsdjup samt ett antal befintliga ledningar av olika slag att ta hänsyn till. Byggnaden ska ha källare och det behöver spontas och pålas. Underlaget till 3D-modelleringen är laserskannade punktmoln samt 2D-underlag som kommunen, Telia och andra ledningsägare tillhandahållit. 
​	Bjerkings geotekniker har även gjort provborrningar. Resultaten från dessa har också modellerats tredimensionellt och visar de olika lagren av exempelvis fyllning, lera och berg. Vid osäkerhet har provgropar schaktats och befintliga ledningar
lokaliserats och karterats inför grundläggningen av byggnaden. Terrängmodellen är baserad på detaljinmätning som är utförd av Bjerking.
​	– Genom att modellera allt i 3D, inklusive spontritning vilket är första gången för Bjerkings, har vi minimerat de geotekniska riskerna i projektet, säger Jonas Gustafsson, entreprenadchef på NCC. Genom att kunna vrida och vända på modellen kan vi exempelvis se hur nära vi kan sponta. Riskminimering innebär samtidigt kostnadsminimering vilket gör att behovet av riskbuffert minskar. Vi får helt enkelt ett säkrare beslutsunderlag och bättre tredimensionell samgranskning av ledningar tillsammans med arkitekt och konstruktör. Eventuella problem löser vi gemensamt.

![Bild 2](media/2.jpeg)

**Genom att vända och vrida på modellen kan man exempelvis se hur nära man kan sponta.**

Genom 3D-modelleringen kan entreprenören i sin schaktplanering studera hur arbetet kan göras mest rationellt. Även schaktplanen har gjorts i 3D. Förutom att åskådliggöra hur gropen ser ut har volymberäkningar gjorts för att visa hur mycket schaktmassor det blir. Lätt att göra, tycker Klas Eckerberg, som totalt sett menar att det varit väldigt lite problem med det tredimensionella arbetet. Men än så länge är det generellt
sett ganska ovanligt att göra schakt- och spontplaner i 3D med tillhörande ledningar.

HAN OCH HANS KOLLEGOR INOM MARK och landskap arbetar i Civil3D, husprojektörerna i Revit och konstruktörerna i AutoCAD. Alla modeller har fogats samman i NavisWorks där man kan se att allt hamnat rätt. Genom att kunna studera allt i tre dimensioner får man en extra koll på att de enskilda delarna fungerar
med varandra.
​	Detaljprojekteringen har påbörjats nu under hösten 2010. Till följd av ett källargarage är det bjälklag på hela gården. Finplanering blir nästa steg att modellera i 3D och troligtvis kommer det även att levereras data för maskinstyrning när det
börjar bli dags att schakta.

KLAS ECKERBERG ANSVARADE FÖR DEL 7 av Bygghandlingar 90, som handlar om redovisning av mark och anläggningar, när den första utgåvan gavs ut för fjorton år sedan. Nu arbetar han med att uppdatera den vilket mycket handlar om digital informationsöverföring – hur man får de digitala modellerna ut på bygget och in i maskinerna.
​	– Mottagligheten och klimatet för BIM är mycket positivt. Just nu driver entreprenörerna på mer än konsulterna, framför allt i infrastrukturprojekt. Tyvärr är byggherrarna där mer försiktiga än byggherrarna på hussidan. För att BIM ska spridas mer behöver administrativa förutsättningar som format och kodning bli bättre. Nu är det för mycket tjafs – alla har allt att vinna på ett större genomslag för BIM, säger Klas Eckerberg.
​	– I innerstadsprojekt har vi sett nyttan av 3D-projektering när man har befintliga byggnader, vägar och ledningar att ta hänsyn till. När så många teknikområden ingår i ett komplicerat byggprojekt är det bra att kunna visualisera detta under projekteringen för att sedan fasa över kunskaperna i produktionen, säger Jonas Gustafsson.